# Vehicle app

### Exercise Requirements

- Read vehicles.txt and create objects of the proper type
- Count the number of cars, motorcycles, tractors
- Count how many vehicles of each brand are there
- Sort the cars by price
- Sort the choppers by top speed
- Display each category of vehicles in separate files

### How to run application
<i>This is console application.</i>

1. Clone or download project.
2. Run Application.java 

### Keywords
<i>OOP, Encapsulation, Collections, Exceptions, Lambda, Streams, IO</i>