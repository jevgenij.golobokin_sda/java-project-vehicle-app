package com.eugenegolobokin.utils;

import java.nio.file.Files;
import java.nio.file.Paths;

public class FileWriter {

    public void writeToFile(String fileName, String content) {
        try {
            Files.write(Paths.get(fileName), content.getBytes());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
