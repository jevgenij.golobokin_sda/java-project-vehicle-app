package com.eugenegolobokin.utils;

import com.eugenegolobokin.model.Car;
import com.eugenegolobokin.model.Motorcycle;
import com.eugenegolobokin.model.Vehicle;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class VehicleHelper {

    public static List<Vehicle> listOfVehicles = new ArrayList<>();

    public int getCountByVehicleType(Class vehicle) {
        return (int) listOfVehicles.stream()
                .filter(vehicle::isInstance)
                .count();
    }

    public void getCountByBrand() {
        Map<String, Long> vehicleCountByBrands = listOfVehicles.stream()
                .collect(groupingBy(Vehicle::getBrand, counting()));

        System.out.println(vehicleCountByBrands);
    }

    public List<Vehicle> getListOfVehiclesByType(Class vehicle) {
        return listOfVehicles.stream()
                .filter(vehicle::isInstance)
                .collect(Collectors.toList());
    }

    public void addToList(Vehicle v) {
        this.listOfVehicles.add(v);
    }

    public void sortCarsByPrice() {
        listOfVehicles.stream()
                .filter(vehicle -> vehicle instanceof Car)
                .filter(vehicle -> vehicle.getPrice() > 0)
                .sorted(Comparator.comparingInt(Vehicle::getPrice))
                .forEach(System.out::println);
    }

    public void sortChoppersBySpeed() {

        listOfVehicles.stream()
                .filter(vehicle -> vehicle instanceof Motorcycle)
                .filter(vehicle -> ((Motorcycle) vehicle).getShape().equalsIgnoreCase("Chopper"))
                .sorted(Comparator.comparingInt(m -> ((Motorcycle) m).getTopSpeed()))
                .forEach(System.out::println);
    }

}
