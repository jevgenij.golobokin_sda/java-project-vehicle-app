package com.eugenegolobokin.utils;

import com.eugenegolobokin.model.Car;
import com.eugenegolobokin.model.Motorcycle;
import com.eugenegolobokin.model.Tractor;


import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CreateObjectsFromFile {

    private final String PATH = "src\\main\\resources\\vehicles.txt";
    private VehicleHelper vehicleHelper = new VehicleHelper();

    public VehicleHelper getVehicleHelper() {
        return vehicleHelper;
    }

    public void readFile() {
        try (Stream<String> vehicleList = Files.lines(Paths.get(PATH))) {
            vehicleList.forEach(line -> createObjectsFromFile(line));

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void createObjectsFromFile(String line) {

        List<String> vehicleProps = Arrays
                .stream(line.split(","))
                .map(String::trim)
                .collect(Collectors.toList());
        switch (vehicleProps.get(0)) {
            case "Car":
                Car car = new Car(vehicleProps.get(1),
                        vehicleProps.get(2),
                        parseInteger(vehicleProps.get(3)),
                        parseInteger(vehicleProps.get(4)),
                        vehicleProps.get(5),
                        vehicleProps.get(6));
                vehicleHelper.addToList(car);
                break;
            case "Motorcycle":
                Motorcycle motorcycle = new Motorcycle(vehicleProps.get(1),
                        vehicleProps.get(2),
                        parseInteger(vehicleProps.get(3)),
                        parseInteger(vehicleProps.get(4)),
                        vehicleProps.get(5));
                vehicleHelper.addToList(motorcycle);
                break;
            case "Tractor":
                Tractor tractor = new Tractor(vehicleProps.get(1),
                        vehicleProps.get(2),
                        parseInteger(vehicleProps.get(3)),
                        parseInteger(vehicleProps.get(4)));
                vehicleHelper.addToList(tractor);
                break;
            default:
                System.out.println("Unknown vehicle type is present in database!\n");
        }
    }

    private int parseInteger(String price) {
        try {
            return Integer.parseInt(price);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }

}
