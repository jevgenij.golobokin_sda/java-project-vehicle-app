package com.eugenegolobokin.model;

public class Motorcycle extends Vehicle {

    private final int topSpeed;
    private final String shape;


    public Motorcycle(String brand, String model, int price, int topSpeed, String shape) {
        super(brand, model, price);
        this.topSpeed = topSpeed;
        this.shape = shape;
    }

    public String getShape() {
        return shape;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    @Override
    public String toString() {
        return brand + ", " + model + ", " + price + ", " + topSpeed + ", " + shape;
    }
}
