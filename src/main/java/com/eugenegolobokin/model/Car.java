package com.eugenegolobokin.model;

public class Car extends Vehicle {

    private final int topSpeed;
    private final String transmission;
    private final String shape;


    public Car(String brand, String model, int price, int topSpeed, String transmission, String shape) {
        super(brand, model, price);
        this.topSpeed = topSpeed;
        this.transmission = transmission;
        this.shape = shape;
    }

    @Override
    public String toString() {
        return brand + ", " + model + ", " + price + ", " + topSpeed + ", " + transmission + ", " + shape;
    }

}
