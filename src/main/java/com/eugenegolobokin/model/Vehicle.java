package com.eugenegolobokin.model;

public abstract class Vehicle {
    String brand;
    String model;
    int price;

    public Vehicle(String brand, String model, int price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return brand + " " + model + " / price = " + price;
    }

}
