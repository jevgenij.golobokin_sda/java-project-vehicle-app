package com.eugenegolobokin.model;

public class Tractor extends Vehicle {

    private final int maxPulledWeight;

    public Tractor(String brand, String model, int price, int maxPulledWeight) {
        super(brand, model, price);
        this.maxPulledWeight = maxPulledWeight;
    }

    @Override
    public String toString() {
        return brand + ", " + model + ", " + price + ", " + maxPulledWeight;
    }
}
