package com.eugenegolobokin;

import com.eugenegolobokin.model.Car;
import com.eugenegolobokin.model.Motorcycle;
import com.eugenegolobokin.model.Tractor;
import com.eugenegolobokin.model.Vehicle;
import com.eugenegolobokin.utils.CreateObjectsFromFile;
import com.eugenegolobokin.utils.FileWriter;
import com.eugenegolobokin.utils.VehicleHelper;

import java.util.List;

public class Application {
    public static void main(String[] args) {

        CreateObjectsFromFile createObjectsFromFile = new CreateObjectsFromFile();
        FileWriter fileWriter = new FileWriter();
        VehicleHelper vehicleHelper = createObjectsFromFile.getVehicleHelper();
        createObjectsFromFile.readFile();

        System.out.println("Cars in list: " + vehicleHelper.getCountByVehicleType(Car.class));
        System.out.println("Motorcycles in list: " + vehicleHelper.getCountByVehicleType(Motorcycle.class));
        System.out.println("Tractors in list: " + vehicleHelper.getCountByVehicleType(Tractor.class));

        System.out.println("\nCount vehicles by brand:");
        vehicleHelper.getCountByBrand();

        System.out.println("\nSorting cars by price:");
        vehicleHelper.sortCarsByPrice();

        System.out.println("\nSorting choppers by top speed:");
        vehicleHelper.sortChoppersBySpeed();

        System.out.println("\nWriting to files...");
        List<Vehicle> cars = vehicleHelper.getListOfVehiclesByType(Car.class);
        List<Vehicle> motorcycles = vehicleHelper.getListOfVehiclesByType(Motorcycle.class);
        List<Vehicle> tractors = vehicleHelper.getListOfVehiclesByType(Tractor.class);

        fileWriter.writeToFile("src\\main\\resources\\cars.txt", cars.toString());
        fileWriter.writeToFile("src\\main\\resources\\motorcycles.txt", motorcycles.toString());
        fileWriter.writeToFile("src\\main\\resources\\tractors.txt", tractors.toString());
    }


}
